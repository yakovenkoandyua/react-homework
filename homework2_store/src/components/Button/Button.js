import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button} from '../../style/index'

class ModalButton extends Component {

    render() {
        const {text, showModal, backgroundColor, className} = this.props;

        return (
            <div>
                <Button className={className} style={{background: backgroundColor}} onClick={showModal}>{text}</Button>
            </div>
        )
    }
}

ModalButton.propTypes = {
    text: PropTypes.string,
    showModal: PropTypes.func,
    background: PropTypes.string
};

export default ModalButton;

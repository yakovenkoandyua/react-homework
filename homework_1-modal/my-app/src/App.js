import React from 'react';
import './App.css';
import Modal from "./components/Modal/Modal";
import ModalButton from "./components/Button/Button";

function App() {
    return (
        <div>
            <Modal
                theme={{main: '#e74c3c', headerColor: '#d44637'}}
                header='Do you want to delete this file?'
                text='Once you delete this file, it won’t be possible to undo this action.
Are you sure you want to delete it?'
                textBtn='Open first modal'
                colorBtn='#ffd700'
                actions={[
                    <ModalButton className='modalBtnFirst' text='Ok'/>,
                    <ModalButton className='modalBtnFirst' text='Cancel'/>]}
            />
            <Modal
                theme={{main: '#09a3b3', headerColor: '#1a656d'}}
                header='Do you want to learn React framework ?'
                text='A JavaScript library for building user interfaces'
                textBtn='Open second modal'
                colorBtn='#adff2f'
                actions={[
                    <ModalButton className='modalBtnSecond' text='Ok'/>,
                    <ModalButton className='modalBtnSecond' text='Cancel'/>]}
            />
        </div>
    );
}


export default App;

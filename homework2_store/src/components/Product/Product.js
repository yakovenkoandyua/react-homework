import React, {Component} from 'react';
import ModalMain from "../Modal/Modal";
import {Section, Article, SubTitle, Icon, IconBlack} from "./StyleProduct";

class Product extends Component {
    state = {
        isActive: true
    };


    addToFavorite() {
        this.setState({isActive: !this.state.isActive});
        const favItem = this.props.item;
        console.log(favItem);
        localStorage.setItem("Card", JSON.stringify(favItem));
    };

    addToBasket() {
        const favItem = this.props.item;
        localStorage.setItem("CardBasket", JSON.stringify(favItem));
    };

    render() {
        const {article, title, src, price, item, addToFav, removeFromFav} = this.props;

        return (
            <Section>
                <Article>Article: {article}</Article>
                <img style={{boxShadow: '0 0 8px 2px antiquewhite'}} src={src} alt=""/>
                <div style={{position: 'relative'}}>
                    <SubTitle>Author: {title}</SubTitle>
                    <span>{price}</span>
                    {this.state.isActive && <Icon onClick={() => {
                        this.addToFavorite();
                        addToFav(item)
                    }} className="far fa-star"/>}

                    {!this.state.isActive && <IconBlack onClick={() => {
                        this.addToFavorite();
                        removeFromFav(item)
                    }} className="fas fa-star"/>}

                    <ModalMain addToLocal={this.addToBasket.bind(this)}
                               colorBtn='#ffd700'
                               theme={{main: 'gold', headerColor: 'darkgoldenrod'}}
                               textBtn='Add to cart'
                               header='Wont to buy this product ?'/>
                </div>
            </Section>
        )
    }
}

export default Product;
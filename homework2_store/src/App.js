import React from 'react';
import './App.css';
import ContainerProduct from "./components/ContainerProduct/ContainerProduct";

function App() {
    return (
        <div>
            <button onClick={()=>{
                localStorage.clear();
            }}>Clear LocalStorage</button>
            <ContainerProduct/>
        </div>
    );
}


export default App;

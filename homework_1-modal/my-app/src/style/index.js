import styled from 'styled-components'

export const Button = styled.button`
    
    margin: 20px;
    padding: 9px 15px;
    font-size: 18px;
    font-weight: 700;
    border: none;
    border-radius: 10px;
    outline: none;
    
        &:active {
        box-shadow: 1px 1px 4px black;;
        }
    `;

export const ModalContent = styled.div`
    max-width: 37%;
    box-shadow: 0 0px 10px 3px rgba(0, 0, 0, 0.3);
    background-color: ${props => props.theme.main};
    border-radius: 8px;
    color: white;
    `;

export const ModalHeader = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    background-color: ${props => props.theme.headerColor};
`;

export const ModalBody = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
`;

export const Title = styled.h1`
    font-size: 22px
    margin: 20px 30px;
`;

export const TextModal = styled.p`
    font-size: 15px;
    line-height: 25px;
    margin: 30px;
    width: 100%;
    text-align: center;
`;

export const Close = styled.span`
    margin-left: auto;
    padding-right: 10px;
    font-size: 30px;
    font-family: cursive;
    transform: rotate(-4deg);
    cursor: pointer;
`;


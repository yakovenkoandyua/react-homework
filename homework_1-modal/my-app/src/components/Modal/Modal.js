import React, {Component} from 'react';
import ModalButton from "../Button/Button";
import PropTypes from 'prop-types';

import {ModalContent, Title, Close, ModalHeader, ModalBody, TextModal} from "../../style";

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: false
        }
    }

    activeModal = () => {
        this.setState({isActive: !this.state.isActive})
    };

    closeModal = () => {
        window.addEventListener('click', (e) => {
                if (e.target === document.querySelector('.modal')) {
                    this.setState({isActive: !this.state.isActive})
                }
            }
        )
    };

    render() {
        const {header, text, actions, textBtn, colorBtn, theme} = this.props;

        return (
            <div>
                <ModalButton showModal={this.activeModal} text={textBtn} backgroundColor={colorBtn}/>
                {this.state.isActive &&
                <div className={'modal'} onClick={this.closeModal}>
                    <ModalContent theme={theme}>
                        <ModalHeader theme={theme}>
                            <Title>{header}</Title>
                            <Close onClick={this.activeModal}>x</Close>
                        </ModalHeader>
                        <ModalBody>
                            <TextModal>{text}</TextModal>
                            {actions[0]}
                            {actions[1]}
                        </ModalBody>
                    </ModalContent>
                </div>}
            </div>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.array,
    textBtn: PropTypes.string
};

export default Modal;

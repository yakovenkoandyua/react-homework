import React, {Component} from 'react';
import {Container} from "../Product/StyleProduct";
import Product from "../Product/Product";

class ContainerProduct extends Component {
    state = {
        data: [],
        favItems: [],
    };

    componentDidMount() {
        fetch('./products.json')
            .then(response => response.json())
            .then(data => {
                this.setState({data: data});
            })
    };

    addToFav = item => {
        this.setState(prevState => ({
            favItems: [...prevState.favItems, item]
        }));
    };

    removeFromFav = item => {
        let idx = item.id;
        let list = this.state.favItems;
        this.setState(() => {
            const favItems = list.filter(item => item.id !== idx);
            return {
                favItems
            };
        });
        localStorage.removeItem("Card")
    };

    render() {
        return (
            <Container>
                {this.state.data.map(i =>
                    <Product
                        title={i.name}
                        key={i.id}
                        article={i.article}
                        item={i}
                        src={i.img}
                        price={i.price}
                        addToFav={this.addToFav}
                        addToCart={this.addToCart}
                        removeFromFav={this.removeFromFav}
                    />)
                }
            </Container>
        )
    }
}

export default ContainerProduct;

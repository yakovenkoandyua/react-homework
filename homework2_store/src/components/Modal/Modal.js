import React, {Component} from 'react';
import ModalButton from "../Button/Button";
import Modal  from 'react-modal';
import PropTypes from 'prop-types';

import {ModalContent, Title, Close, ModalHeader, ModalBody} from "../../style";


class ModalMain extends Component {
    state = {
        isActive: false
    };

    handleModal = () => {
        this.setState({isActive: !this.state.isActive})
    };

    render() {
        const {header, textBtn, colorBtn, theme, addToLocal} = this.props;
        return (
            <div>
                <ModalButton showModal={this.handleModal} text={textBtn} backgroundColor={colorBtn}/>

                <Modal //решил протестировать модальное окно от Реакта, но почему-то не работает закрытие модалки по КЛИКУ на background
                    isOpen={this.state.isActive}
                    ariaHideApp={false}
                    shouldFocusAfterRender={false}
                    closeTimeoutMS={200}
                    overlayClassName='modal'
                    className='modal'
                    onRequestClose={this.handleModal}
                >
                    <ModalContent theme={theme}>
                        <ModalHeader theme={theme}>
                            <Title>{header}</Title>
                            <Close onClick={this.handleModal}>x</Close>
                        </ModalHeader>
                        <ModalBody>
                            <ModalButton backgroundColor={'darkgoldenrod'} className='modalBtnFirst' showModal={()=>{
                                addToLocal();
                                this.handleModal()
                            }} text='Add'/>
                            <ModalButton backgroundColor={'darkgoldenrod'} className='modalBtnFirst' showModal={this.handleModal} text='Cancel'/>
                        </ModalBody>
                    </ModalContent>
                </Modal>
            </div>
        )
    }
}

ModalMain.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.array,
    textBtn: PropTypes.string
};

export default ModalMain;

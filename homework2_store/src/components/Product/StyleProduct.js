import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    width: 1180px;
    margin: 0 auto;
    `;

export const Section = styled.div`
    margin: 10px;
    border: 2px solid #000;
    padding: 0 10px;
    border-radius: 3px;
    background-color: #000000d4;
    color: #fff;
`;

export const Article = styled.p`
    font-size: 13px;
    font-weight: 600;
    margin: 8px 0;
`;

export const SubTitle = styled(Article)`
    font-size: 17px;
    font-family: sans-serif;
    letter-spacing: 0.3px;
`;

export const Icon = styled.i`
    position: absolute;
    top: 0px;
    right: 0px;
    padding: 10px;
    font-size: 20px;
    box-shadow: 0px 0px 4px white;
    border-radius: 50%;
`;

export const IconBlack = styled(Icon)`
    border: 1px solid blanchedalmond;
    box-sizing: border-box;
`;
